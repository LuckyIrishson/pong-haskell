# pong-haskell

 Implementing pong with gloss as detailed here 
 http://andrew.gibiansky.com/blog/haskell/haskell-gloss/ to become familiar with gloss.

 ## Instructions

 Requires stack and then simply run the following commands in the directory:

 > stack build

 > stack exec pong-haskell-exe