module Main where

import Lib
import System.Exit
import Control.Monad.Random
import Data.Set
import Graphics.Gloss
import Graphics.Gloss.Data.ViewPort
import Graphics.Gloss.Interface.IO.Game

width, height, offset :: Int
width = 800
height = 800
offset = 100

window :: Display
window = InWindow "Pong" (width, height) (offset, offset)

background :: Color
background = black

fps :: Int
fps = 60

walloffset = 200
paddleWidth = 80
paddleDepth = 20
courtLength = 400

minVelocity = 100 :: Float
maxVelocity = 150 :: Float
maxScore = 5

type Radius = Float
type Position = (Float, Float)

data PongGame = Game
    { ballLocation :: Position
    , ballVelocity :: Position --Velocity is just an (x, y) tuple as well
    , player1 :: Position 
    , player2 :: Position
    , paddlesAction :: Set GameAction
    , score :: (Int, Int)
    , paused :: Bool
    } deriving Show

data GameAction = Paddle1Up | Paddle1Down | Paddle2Up | Paddle2Down | NoAction deriving (Eq, Ord, Enum, Show)

initialState :: Float -> Float -> PongGame
initialState vx vy = Game
    { ballLocation = (0, 0)
    , ballVelocity = (vx, vy)
    , player1 = (courtLength / 2, 0)
    , player2 = (-courtLength / 2, 0)
    , paddlesAction = empty
    , score = (0, 0)
    , paused = False
    }

swing :: (Ord a, Fractional a, Num b) => a -> b -> b
swing a b
  | a <= 0.5 = -b
  | a > 0.5 = b
  | otherwise = b


generateBallVelocity :: Float -> Float -> StdGen -> [Position]
generateBallVelocity a b sg = zip x' y'
  where
    (sg1, sg2) = Control.Monad.Random.split sg
    x = randomRs (a, b) sg1
    y = randomRs (a, b) sg2
    p = randomRs (0, 1) sg1 :: [Float]
    x' = [swing p_n x_n | (x_n, p_n) <- zip x p]
    q = randomRs (0, 1) sg2 :: [Float]
    y' = [swing q_n y_n | (y_n, q_n) <- zip y q]



generateTuples :: Int -> Float -> Float -> StdGen -> [Position]
generateTuples n a b sg = Prelude.take n $ generateBallVelocity a b sg

moveBall :: Float -> PongGame -> PongGame
moveBall seconds game = game { ballLocation = (x', y') }
  where
    (x, y) = ballLocation game
    (vx, vy) = ballVelocity game
    x' = x + vx * seconds
    y' = y + vy * seconds

movePaddle :: PongGame -> PongGame
movePaddle game = game { player1 = (x1, y1'), player2 = (x2, y2') }
  where
    (x1, y1) = player1 game
    (x2, y2) = player2 game
    y1'
     | Data.Set.member Paddle1Up (paddlesAction game) = y1 + 2
     | Data.Set.member Paddle1Down (paddlesAction game) = y1 - 2
     | otherwise = y1
    y2'
     | Data.Set.member Paddle2Up (paddlesAction game) = y2 + 2
     | Data.Set.member Paddle2Down (paddlesAction game) = y2 - 2
     | otherwise = y2

wallCollision :: Position -> Radius -> Bool
wallCollision (_, y) radius = topCollision || bottomCollision
  where
    topCollision = y - radius <= (-walloffset)
    bottomCollision = y + radius >= walloffset

wallBounce :: PongGame -> PongGame
wallBounce game = game { ballVelocity = (vx, vy') }
  where
    radius = 10
    (vx, vy) = ballVelocity game --old velocity

    vy' = if wallCollision (ballLocation game) radius
          then
            -vy
          else
            vy

paddleCollision :: Position -> Position -> Position -> Radius -> Int
paddleCollision (ballX, ballY) (paddle1X, paddle1Y) (paddle2X, paddle2Y) radius
  | player1Collision = 1
  | player2Collision = 2
  | otherwise = 0
   where
    paddleYcollide paddle = (ballY - paddle + (paddleWidth / 2) >= -radius ) && (ballY - paddle - paddleWidth / 2 <= radius)
    player1Collision = ballX + radius - paddle1X - (paddleDepth / 2) >= 0 && paddleYcollide paddle1Y
    player2Collision = ballX - radius - paddle2X + (paddleDepth / 2) <= 0 && paddleYcollide paddle2Y

paddleBounce :: PongGame -> PongGame
paddleBounce game = game { ballVelocity = (vx', vy')}
  where
    radius = 10
    c = paddleCollision (ballLocation game) (player1 game) (player2 game) radius
    v = ballVelocity game
    (vx', vy')
      | c == 1 = paddleReflect (snd $ player1 game) (ballLocation game) v
      | c == 2 = paddleReflect (snd $ player2 game) (ballLocation game) v
      | otherwise = v
    

--For more traditional behaviour, if the ball hits the paddle's top half it will bounce upwards, bottom-half
--bounce downwards
paddleReflect :: Float -> Position -> Position -> Position
paddleReflect paddleY (ballX, ballY)  (vx, vy)
  | ballY > paddleY && vy > 0 = (-vx, vy)
  | ballY > paddleY && vy < 0 = (-vx, -vy)
  | ballY < paddleY && vy > 0 = (-vx, -vy)
  | ballY < paddleY && vy > 0 = (-vx, vy)
  | otherwise = (-vx, vy)

checkForGoal :: [Position] -> Float -> PongGame -> PongGame
checkForGoal r seconds game = game { score = (s1', s2'), ballLocation = (ballX', ballY'), ballVelocity = (ballVX', ballVY')}
       where
       radius = 10
       (s1, s2) = score game
       (g1x, _) = player1 game
       (g2x, _) = player2 game
       (ballX, ballY) = ballLocation game
       s1'
         | ballX - radius > g1x = s1 + 1
         | otherwise = s1
       s2'
         | ballX + radius < g2x = s2 + 1
         | otherwise = s2
      -- If there is a goal reset location and set random velocity
       (ballX', ballY')
         | s1' > s1 || s2' > s2 = (0, 0)
         | otherwise = ballLocation game
       (ballVX', ballVY') 
         | s1' > s1 || s2' > s2 = r !! (s1' + s2')
         | otherwise = ballVelocity game

marchBall :: Float -> Position -> Position -> Position
marchBall goalX currPos ballVel
  | fst currPos' == goalX = currPos' -- Where the ball is predicted to go on the goaline
  | fst currPos' == 0 = currPos' -- Ball is going towards opposition player (no action required, give impossible value x=0)
  | otherwise = marchBall goalX currPos' ballVel'
        where
          radius = 10
          (ballX, ballY) = currPos
          (ballVx, ballVy) = ballVel
          tyUp = ((-walloffset) + radius - ballY) / ballVy
          tyDown = (walloffset - radius - ballY) / ballVy
          tg = (goalX - ballX) / ballVx
          (currPos', ballVel')
            | tyUp > 0 && tyUp < tg = ((ballX + tyUp * ballVx, ballY + tyUp * ballVy), (ballVx, -ballVy))
            | tyDown > 0 && tyDown < tg = ((ballX + tyDown * ballVx, ballY + tyDown * ballVy), (ballVx, -ballVy))
            | tg > 0 = ((ballX + tg * ballVx, ballY + tg * ballVy), (ballVx, -ballVy))
            | otherwise = ((0,0), (0,0))

stepAI :: PongGame -> PongGame
stepAI game = game { paddlesAction = pa' }
        where
          (g1x, _) = player1 game
          (pX, pY) = marchBall g1x (ballLocation game) (ballVelocity game)
          (paddleX, paddleY) = player1 game
          pa = paddlesAction game
          pa'
            | (pX == paddleX) && (pY < paddleY) && Data.Set.notMember Paddle1Down pa = Data.Set.insert Paddle1Down pa 
            | (pX == paddleX) && (pY > paddleY) && Data.Set.notMember Paddle1Up pa = Data.Set.insert Paddle1Up pa 
            | otherwise = Data.Set.delete Paddle1Down $ Data.Set.delete Paddle1Up pa

render :: PongGame -> IO Picture
render game = return (Pictures [ 
    ball,
    walls,
    mkPaddle rose player1X player1Y,
    mkPaddle orange player2X player2Y
    ])
    where
        ball = uncurry Translate (ballLocation game) $ Color ballColour $ circleSolid 10
        ballColour = dark red

        wall :: Float -> Picture
        wall offset = Translate 0 offset $ Color wallColour $ rectangleSolid courtLength 10

        wallColour = greyN 0.5
        walls = Pictures [wall walloffset, wall (-walloffset)]

        (player1X, player1Y) = player1 game
        (player2X, player2Y) = player2 game

        mkPaddle :: Color -> Float -> Float -> Picture
        mkPaddle col x y = Pictures [ 
            Translate x y $ Color col $ rectangleSolid 26 86,
         Translate x y $ Color paddleColour $ rectangleSolid paddleDepth paddleWidth ]

        paddleColour = light (light blue)

handleKeys :: Event -> PongGame -> IO PongGame
handleKeys (EventKey (Char 'p') Down _ _ ) game = return game { paused = not $ paused game }
handleKeys (EventKey (Char 'w') Up _ _ ) game = return game { paddlesAction = Data.Set.delete Paddle2Up (paddlesAction game) }
handleKeys (EventKey (Char 'w') Down _ _ ) game = return game { paddlesAction = Data.Set.insert Paddle2Up (paddlesAction game) }
handleKeys (EventKey (Char 's') Up _ _ ) game = return game { paddlesAction = Data.Set.delete Paddle2Down (paddlesAction game) }
handleKeys (EventKey (Char 's') Down _ _ ) game = return game { paddlesAction = Data.Set.insert Paddle2Down (paddlesAction game) }
--handleKeys (EventKey (SpecialKey KeyUp) Up _ _ ) game = return game { paddlesAction = Data.Set.delete Paddle1Up (paddlesAction game) }
--handleKeys (EventKey (SpecialKey KeyUp) Down _ _ ) game = return game { paddlesAction = Data.Set.insert Paddle1Up (paddlesAction game) }
--handleKeys (EventKey (SpecialKey KeyDown) Up _ _ ) game = return game { paddlesAction = Data.Set.delete Paddle1Down (paddlesAction game) }
--handleKeys (EventKey (SpecialKey KeyDown) Down _ _ ) game = return game { paddlesAction = Data.Set.insert Paddle1Down (paddlesAction game) }
--handleKeys (EventKey (Char 'g') _ _ _) game = return game { ballLocation = (0, 0), ballVelocity = (50, 0) } --Reset ball
handleKeys _ game = return game

update :: [Position] -> Float -> PongGame -> IO PongGame
update r seconds game
  | fst (score game) == maxScore = exitSuccess
  | snd (score game) == maxScore = exitSuccess
  | paused game = return game
  | otherwise = return $ checkForGoal r seconds . stepAI . movePaddle . wallBounce . paddleBounce $ moveBall seconds game

main :: IO ()
main = do
  g <- newStdGen
  let (vx, vy) = head $ Prelude.take 1 $ generateBallVelocity minVelocity maxVelocity g
  let r = generateTuples (2 * maxScore - 1) minVelocity maxVelocity g-- Multiply by ten for more variety when maxScore is small
  playIO window background fps (initialState vx vy) render handleKeys (update r)

